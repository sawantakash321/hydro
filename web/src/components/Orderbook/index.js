import React from 'react';
import { connect } from 'react-redux';
// import BarChart from '../BarChart'
import './styles.scss';
import styled from "styled-components"


const Bar = styled.div`
  height: 120%;
  bottom: 0px;
  right: 0;
  position: absolute;
  background: rgb(204, 227, 241);
  z-index: 1;
  minWidth: ${props => props.percent || '20%'};
  &:last-child {
    border-bottom: none;
  }
`

class OrderBook extends React.Component {
  constructor(props) {
    super(props);
    this.lastUpdatedAt = null;
    this.forceRenderTimer = null;
  }
 
  // max 1 render in 1 second
  shouldComponentUpdate() {
    if (this.lastUpdatedAt) {
      const diff = new Date().valueOf() - this.lastUpdatedAt;
      const shouldRender = diff > 1000;

      if (!shouldRender && !this.forceRenderTimer) {
        this.forceRenderTimer = setTimeout(() => {
          this.forceUpdate();
          this.forceRenderTimer = null;
        }, 1000 - diff);
      }
      return shouldRender;
    } else {
      return true;
    }
  }

  componentWillUnmount() {
    if (this.forceRenderTimer) {
      clearInterval(this.forceRenderTimer);
    }
  }

  componentDidUpdate() {
    this.lastUpdatedAt = new Date();
  }

  render() {
    let { bids, asks, websocketConnected, currentMarket } = this.props;
    console.log('props',this.props);

    return (
      <div className="orderbook flex-column flex-1">
        <div className="flex header text-secondary">
          <div className="col-4 text-center"> <b>Amount</b> </div>
          <div className="col-4 text-center"><b>Price</b></div>
          <div className="col-4 text-center">-  </div>
        </div>
        <div className="flex-column flex-1">
          <div className="asks flex-column flex-column-reverse">
            {asks
              .slice(-20)
              .reverse()
              .toArray()
              .map(([price, amount], index) => {
                // const barSize = (price/amount ) * 10000;  
                return (
                  <div className={`ask flex align-items-center ${index%2 && 'orderbook--zebraGray'}`} key={price.toString()}>
                    <div className="col-4 orderbook-amount text-left">
                      <Bar percent={'20%'} />
                      <div className="orderbook-amount-value">{amount.toFixed(currentMarket.amountDecimals)} </div>
                    </div>
                    <div className="col-4 text-danger text-center orderbook--opacityGray"><div><b>{price.toFixed(currentMarket.priceDecimals)}</b>
                    </div><div className="orderbook--currency">79 <b>USD</b></div>
                    </div>
                    <div className="col-4 orderbook-amount text-center">
                    -
                    </div>
                  </div>
                );
              })}
          </div>
          <div className="status border-top border-bottom">
            {websocketConnected ? (
              <div className="col-6 text-success">
                <i className="fa fa-circle" aria-hidden="true" /> RealTime
              </div>
            ) : (
              <div className="col-6 text-danger">
                <i className="fa fa-circle" aria-hidden="true" /> Disconnected
              </div>
            )}
          </div>
          <div className="bids flex-column flex-1">
            {bids
              .slice(0, 20)
              .toArray()
              .map(([price, amount], index) => {
                // const barSize = (price/amount ) * 10000; 
                return (
                  <div className={`bid flex align-items-center ${index%2 && 'orderbook--zebraGray'}`} key={price.toString()}>
                    <div className="col-4 orderbook-amount text-center">
                    <Bar percent={'20%'} />
                      <div className="orderbook-amount-value">{amount.toFixed(currentMarket.amountDecimals)}</div>
                    </div>
                    <div className="col-4 text-success text-center orderbook--opacityGray"><div><b>{price.toFixed(currentMarket.priceDecimals)}</b></div>
                    <div className="orderbook--currency">79 <b>USD</b></div>
                    </div>
                    <div className="col-4 text-center">
                    -
                    </div>
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    asks: state.market.getIn(['orderbook', 'asks']),
    bids: state.market.getIn(['orderbook', 'bids']),
    loading: false,
    currentMarket: state.market.getIn(['markets', 'currentMarket']),
    websocketConnected: state.config.get('websocketConnected'),
    theme: state.config.get('theme')
  };
};

export default connect(mapStateToProps)(OrderBook);
